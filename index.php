<?

require 'vendor/autoload.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/mustache'),
]);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Успешная оплата</title>
</head>
<body>

    <header class="header">
        <div class="menu_burger"><span></span></div>
        <p class="basket"><i class="icon-basket"></i> 0</p>
    </header>

    <? echo $mustache->render('payment_state', include $_SERVER['DOCUMENT_ROOT'] . '/template information/payment_state_info.php') ?>

    <? echo $mustache->render('services', include $_SERVER['DOCUMENT_ROOT'] . '/template information/service_info.php') ?>

</body>
</html>