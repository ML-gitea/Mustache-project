<?
    $context = [
        'title' => 'SOME OF MY SERVICES',
        'cards' => [
            [
                'image_url' => 'image/Prom.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => 'from 30$',
                'button_name' => 'Book online >',
                'button_url' => '#'
            ],
            [
                'image_url' => 'image/Prom.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => '30$',
                'button_name' => 'Book online >',
                'button_url' => '#'
            ],
            [
                'image_url' => 'image/img.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => '150$',
                'button_name' => 'Записаться >',
                'button_url' => '#'
            ],
            [
                'image_url' => 'image/Prom.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => 'from 30$',
                'button_name' => 'Book online >',
                'button_url' => '#'
            ],
            [
                'image_url' => 'image/img.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => '150$',
                'button_name' => 'Записаться >',
                'button_url' => '#'
            ],
            [
                'image_url' => 'image/Prom.jpg',
                'services_name' => 'Hollywood Laser Spectra Peel',
                'price' => 'from 30$',
                'button_name' => 'Book online >',
                'button_url' => '#'
            ] 
        ]
    ];

    $context['empty'] = count($context['cards']) === 0;

    return $context;
?>